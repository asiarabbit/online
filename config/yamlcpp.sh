#!/bin/bash
# note that execution of this script requires root privileges.

# yaml-cpp enables yaml-format input in cnok
# which is a very conenient and elegent way to collect user-configuration input
# it can be installed via package managers (dnf, apt, yum, etc.)
# in most linux systems (e.g., Fedora and Ubuntu)
# for those that cannot (e.g., CentOS 7), please use this shell script to enable
# `catch` in your computer

# either the https://github.com/jbeder/yaml-cpp -- original repository
# or the https://gitee.com/garricklin/yaml-cpp -- mirror of the above
# is OK. The latter is much faster for users located in China
# NOTE that cmake-3.4 or higher is requird to build yaml-cpp (Dec. 2022)

git clone -b master --depth=1 https://gitee.com/garricklin/yaml-cpp
cd yaml-cpp
cmake -Bbuild -H. -DBUILD_TESTING=OFF
sudo cmake --build build/ --target install
