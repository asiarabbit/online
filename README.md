# skt

#### Description
Online - the client peer for GDAQ system to transmit data from daq host to ana
host. Written for the External Target Facility in HIRFL-CSR, IMP-CAS, Lanzhou.

## Prerequisites ##
The code relies `yaml-cpp` to work. In cases where the Linux distribution does
not provide yaml-cpp in its package, one can download the source and compile &
install to your computer by running the shell script `config/yaml-cpp.sh`.

## Installation ##
0. Download the code
- `git clone -b master --depth=1 https://gitee.com/asiarabbit/online`
1. Make a build directory
- `make build && cd build`
2. Do cmake
- `../cmake.sh`
3. Do make
- `make`
4. Then just run the program
- `conline`

## Configuration IP ##
Just specify the ip:port in `config/ip.yaml`.

### For developers ###
The main executable `conline` (standing for *client online*) is compiled from user
application `src/conline.cxx`. Advanced user intentions are completed by changing
this file.

## The Mailing List ##
For any question, please contact the Author: Asia SUN via
[asisrabbit@163.com](asisrabbit@163.com).
