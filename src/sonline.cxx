/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \brief online via LAN, the Server part - transmit data from daq shm to a server
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-08-02
  \date 2023-08-02 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                   </tr>
  <tr>  <td>2023-08-02   <td>Asia Sun    <td>file created                  </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#include "ETFYaml.h"
#include "ETFReadShm.h"
#include "ETFServer.h"
#include "ETFMsg.h"

string ipconfig = "ipc.yaml";

int main(int argc, char *argv[]){
  ETFMsg::Logo();
  if(argc > 1) ipconfig = argv[1];
  // add daq sources //
  const ETFYaml cc(ipconfig);
  if(!cc["daq"]){
    ETFMsg::Error("sonline", "no daq items in %s", ipconfig.data());
    exit(1);
  } // end if
  const int nshm = cc["nshm"] ? cc["nshm"].as<int>() : 0; // nof shms out
  vector<ETFServer *> vs; // the servers
  for(auto &p : cc["daq"]){
    const DaqType type = p["type"].as<bool>() ? kPXI : kVME;
    const string shm = p["shm"].as<string>();
    const string ip = p["ipport"][0].as<string>();
    const short port = p["ipport"][1].as<short>();
    ETFServer *s = new ETFServer(ip, port, shm, type, nshm);
    if(s->Online()) vs.push_back(s);
    else delete s;
  } // end for over daqs
  for(auto &p : vs) p->Initialize(); // launch the reading threads

  while(!ETFMsg::irp()) for(auto &p : vs) p->send();

  for(auto &p : vs) delete p;
  ETFMsg::Logo();

  return 0;
} // end main
