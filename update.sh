#########################################################################
# File Name: update.sh
# Brief: update the code to the cmc host
# Author: asia.rabbit
# mail: asia.rabbit@163.com
# Created Time: Tue 08 Aug 2023 01:17:18 PM CST
#########################################################################
#!/bin/bash

#scp -r CMakeLists.txt cmake.sh config LICENSE online README.md src cmc:/home/asia/online/online/
# mkdir -p config online src
scp -r `ls` cmc:/home/asia/online/online/
scp -r `ls` f1:/home/asia/online/online/
