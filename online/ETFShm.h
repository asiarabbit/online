/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFShm
  \brief shared memory class. Supposed to open and read existing shm for online
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-08
  \date 2023-06-08 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-08   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef ETFShm_h
#define ETFShm_h

#include <sys/stat.h>
#include <string>
#include "DaqType.h"
#include "online_shm.h"

using std::string;

class ETFShm{
public:
  ETFShm();
  /// \brief create posix shm with name
  /// \param isCreate whether to create shm if not exists
  ETFShm(const string &name, DaqType daqType = kPXI, bool isCreate = false);
  virtual ~ETFShm();
  
  void *Buffer() const{ return fBuffer; }
  void *buf() const{ return fBuffer; }
  const char *GetName() const{ return fName.data(); }
  static bool exist(const string &name); ///< to tell if a sem exists

  int Close(); ///< unmap the shm
  int Delete(); ///< delete the shm

protected:
  void *fBuffer; ///< pointer to the shm
  string fName; ///< name of the shm
  struct stat fStat; ///< attribute of the shm
  int fDaqType;
  bool fIsCreate; ///< whether the shm is create by this
};

#endif
