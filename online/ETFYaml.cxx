/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFYaml
  \brief a YAML user input reader
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2021-10-27
  \date 2021-11-24 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                   </tr>
  <tr>  <td>2021-10-27   <td>Asia Sun    <td>file created                  </tr>
  </table>

  \copyright Copyright (c) 2021-2023 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#include "ETFYaml.h"

ETFYaml::ETFYaml(){}
ETFYaml::ETFYaml(const string &file) : YAML::Node(YAML::LoadFile(file.c_str())){}
ETFYaml::ETFYaml(const YAML::Node &n) : YAML::Node(n){}
// ETFYaml::ETFYaml(const ETFYaml &n) : YAML::Node(){
//   if(this == &n) return;
//   *this = n;
// } // end copy ctor


ETFYaml::~ETFYaml(){}
