/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \struct online_shm_pxi
  \brief vme online structure defined by S.T. Wang for ETF
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-07-07
  \date 2023-07-07 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-07-07   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef online_cmd_h
#define online_cmd_h

/// commands to be passed from user to the daq side
enum online_cmd{ONLINE_IDLE = 0, ONLINE_INIT_DAQ_SHM, ONLINE_CLOSE, ONLINE_ACK,
  ONLINE_SHM_STATUS};

#endif
