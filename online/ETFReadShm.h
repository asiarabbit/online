/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFReadShm
  \brief read data from daq shm, and write to ETFQueue for further processing
  Supposed to be a tool class
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-08
  \date 2023-06-08 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-08   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef ETFReadShm_h
#define ETFReadShm_h

#include <string>
#include <signal.h>
#include "ETFDaqData.h"

using std::string;

class ETFShm;
struct online_shm;

class ETFReadShm : public ETFDaqData{
public:
  ETFReadShm();
  /// \param name the posix shm name to read from
  /// \param isCreate whether to create shm if not exists
  ETFReadShm(const string &name, DaqType daqType = kPXI, bool isCreate = false);
  virtual ~ETFReadShm();

  void CreateShm(const string &name, bool isCreate = false);
  void CleanShm(); // just recover last abnormal exit (mainly do some sem_post's)
  ETFShm *GetShm() const{ return fShm; }
  ETFShm *shm() const{ return fShm; }
  online_shm *buf() const{ return fBuf; }
  int nstored() const{ return peeksem(fBuf->nstored); }
  int nempty() const{ return peeksem(fBuf->nempty); }
  int mutex() const{ return peeksem(fBuf->mutex); }
  virtual void ShmStatus(); ///< print shm status

  virtual void Initialize() override; // initiate the thread
  virtual void Close() override; ///< close the shm or socket
  /// initialize the shared memory \param reinit whether 1st init or not
  void InitShm(bool reinit = false);

  void read(); ///< read from daq shm
  // write 1 event to daq shm - sim daq to debug online
  void write(const void *src); ///< note that this function does not goes into any thread

  static bool exist(const string &name); ///< to tell if a sem exists
  static int peeksem(sem_t &s); ///< utility function, e.g., for gdb

  // facilities to facilitate immediate reaction to ctrl-C //
  friend void *handle_ctrlc_readShm(void *qq);
  friend void handler_readShm(int sig, siginfo_t *info, void *context);

protected:
  /// thread func, made member to avoid namespace contamination
  static void *avatar(void *rshm);

  ETFShm *fShm; ///< pointer to the daq shm
  online_shm *fBuf; ///< pointer to the buffer of fShm
  int fMAX_NUM, fMAX_LEN; ///< fBuf->MAX_NUM, fBuf->MAX_LEN
};

#endif
