/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFClient
  \brief client for communication among different hosts on the Internet
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-21
  \date 2023-06-21 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-21   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef ETFClient_h
#define ETFClient_h

#include "ETFVClient.h"
#include <vector>

using std::vector;

class ETFQueue;
class ETFReadShm;

class ETFClient : public ETFVClient{
public:
  ETFClient();
  /// establish connection with ip:port
  ETFClient(const string &ip, short port, DaqType daqType = kPXI, int nshm = 0);
  virtual ~ETFClient();

  virtual void Initialize() override;
  virtual void Close() override; ///< close the object
  virtual void recv() override; ///< receive data from the Internet

protected:
  int fNShm; ///< max nof shms for porting out
  vector<ETFReadShm *> fShms; ///< porting out to shms
};

#endif
