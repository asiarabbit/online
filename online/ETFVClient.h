/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFVClient
  \brief client for communication among different hosts on the Internet.
  NOTE that this is an abstract class
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-09
  \date 2023-06-09 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-09   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef ETFVClient_h
#define ETFVClient_h

#include <netinet/in.h>
#include <string>
#include "ETFDaqData.h"

class ETFQueue;

using std::string;

class ETFVClient : public ETFDaqData{
public:
  ETFVClient();
  /// establish connection with ip:port
  ETFVClient(const string &ip, short port, DaqType daqType = kPXI);
  virtual ~ETFVClient();

  virtual void Initialize() override;
  /// establish connection with ip:port
  virtual bool Connect();
  virtual void Close() override; ///< close the shm or socket
  bool Connected() const{ return fIsConnected; }
  virtual void recv() = 0; ///< receive data from the Internet

protected:
  static void *avatar(void *arg); ///< thread func to read from LAN

  /// ip:port of the server ///
  string fIP;
  short fPort;
  bool fIsConnected;

  sockaddr_in fSock;
  int fd; ///< the file descriptor of the socket
};

#endif
