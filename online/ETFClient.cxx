/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFClient
  \brief client for communication among different hosts on the Internet
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-21
  \date 2023-06-25 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-21   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#include <iostream>
#include <unistd.h> // for compatibility with older linux versions
#include "ETFClient.h"
#include "ETFQueue.h"
#include "ETFMsg.h"

using std::cout;
using std::endl;

ETFClient::ETFClient(){}
/// establish connection with ip:port
ETFClient::ETFClient(const string &ip, short port, DaqType daqType, int nshm)
 : ETFVClient(ip, port, daqType), fNShm(nshm){
  fQueue = new ETFQueue(daqType);
} // end ctor

ETFClient::~ETFClient(){
  Close();
} // end dtor

// receive data from sock of size bytes and dispose it
void ETFClient::recv(){
  while(!fQueue->Add(fd)) usleep(10);
  for(auto &p : fShms) p->write(fQueue->ItemIn()->buf);
} // end member function recv

void ETFClient::Initialize(){
  // porting out to an shm for others to use //
  char shm[128];
  for(int i = 0; i < fNShm; i++){
    sprintf(shm, "etfanaC_%s%d", fDaqType == kPXI ? "pxi" : "vme", i);
    fShms.push_back(new ETFReadShm(shm, fDaqType, true)); // true: create shm
  } // end for over shms
  ETFVClient::Initialize(); // initiate the reading thread - has to run after fShms
} // end member function Initialize

void ETFClient::Close(){
  for(auto &p : fShms) if(p){ delete p; p = nullptr; }
} // end member function Close
