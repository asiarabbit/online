/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \struct item
  \brief item for ETFQueue
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-08
  \date 2023-06-08 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-08   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/


#ifndef item_h
#define item_h

struct item{
  int len; ///< length of buf in word (buf = new int[len])
  int size; ///< the size of effective data in byte
  int idx; ///< index: the identifier of the data in the buf, e.g., event index
  int *buf;

  item() : len(0), size(0), idx(0), buf(0){}
  ~item(){ free(); }

  void free(){
    if(buf){
      delete [] buf;
      buf = nullptr;
    } // end if
  } // end member function free

  void initialize(int s){
    if(buf && s != len){
      delete [] buf;
      buf = nullptr;
    } // end if
    if(!buf && s > 0){
      buf = new int[s];
      len = s;
    } // end if
    memset(buf, 0, sizeof(int)*len);
    if(size) size = 0;
    idx = -1;
  } // end member function initialize
};

#endif
