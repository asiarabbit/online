/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \enum DaqType
  \brief as the name indicates
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-07-08
  \date 2024-07-22 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-07-08   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef DaqType_h
#define DaqType_h

// MPROOT: root file input from P. Ma
enum DaqType : short{ kVME = 0, kPXI = 1, kMPROOT, kNDFDaqType }; // NDF: not defined

#endif
