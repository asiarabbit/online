/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFDaqData
  \brief providing a general interface to ETFOnlineSource, shielding the underlying
  data generating mechanism, i.e., shm or socket
  This is supposed to be the base class of socket (client, server) and shm
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-24
  \date 2023-06-24 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-24   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#include <algorithm>
#include "ETFDaqData.h"
#include "ETFQueue.h"
#include "ETFMsg.h"

ETFDaqData::ETFDaqData(DaqType daqType) : fQueue(0), fIsRecvThAlive(false),
fIsSendThAlive(false), fDaqType(daqType){
  fsa.sa_flags = SA_SIGINFO; // specifying using sa_sigaction explicitly
  sigemptyset(&fsa.sa_mask);
  if(kPXI == fDaqType) strcpy(fDaqC, "PXI");
  else strcpy(fDaqC, "VME");
} // end ctor

ETFDaqData::~ETFDaqData(){}

// get a queue of size bytes in dst
// idx: index of the obtained event
void ETFDaqData::Get(void *dst, unsigned &idx){
  if(!fQueue) ETFMsg::Error("ETFDaqData", "Get: fQueue not assigned.");
  fQueue->Pop(dst, idx);
} // end member function Get

// step forward (consume) n items
void ETFDaqData::Drop(int n){
  if(n <= 0) return;
  static unsigned idx;
  static int dst[online_shm_pxi::MAX_LEN];
  int nn = std::min(n, loaded());
  for(int i = 0; i < nn; i++) Get(dst, idx);
} // end member function Get

// reset the queue
void ETFDaqData::Reset(){
  if(fQueue) fQueue->Reset();
} // end member function Reset

int ETFDaqData::loaded() const{
  return fQueue->NLoaded();
} // end member function loaded