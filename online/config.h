/**
	ETFAna project, Anyang Normal University && IMP-CAS
  \brief just an empty file - we didn't define anything here
	\author SUN Yazhou, asia.rabbit@163.com
  \since 2023-08-09
  \date 2023-08-09 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                   </tr>
  <tr>  <td>2023-08-09   <td>Asia Sun    <td>file created                  </tr>
  </table>

  \copyright Copyright (c) 2021-2023 IMP-CAS with LGPLv3 LICENSE
*/

#ifndef CONFIG_H
#define CONFIG_H

// for switch on cpp unittest -- catch2, mostly for debugging purposes only
// #define _ROOT_

#endif
