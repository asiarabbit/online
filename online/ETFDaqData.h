/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFDaqData
  \brief providing a general interface to ETFOnlineSource, shielding the underlying
  data generating mechanism, i.e., shm or socket
  This is supposed to be the base class of socket (client, server) and shm
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-24
  \date 2023-06-24 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-24   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef ETFDaqData_h
#define ETFDaqData_h

#include <signal.h>
#include <pthread.h>
#include "DaqType.h"
#include "online_shm.h"

class ETFQueue;

class ETFDaqData{
public:
  ETFDaqData(DaqType daqType = kPXI);
  virtual ~ETFDaqData();

  virtual void Initialize() = 0; ///< start the reading thread
  virtual void Close() = 0; ///< close the shm or socket
  virtual void Reset(); ///< reset the queue

  /// \brief get a queue of size bytes in dst
  /// \param idx index of the obtained event
  void Get(void *dst, unsigned &idx);
  void Drop(int n); ///< step forward (consume) n items
  DaqType daqType() const{ return fDaqType; }
  ETFQueue *queue() const{ return fQueue; }
  ETFQueue *GetQueue() const{ return fQueue; }
  int loaded() const; ///< retval: fQueue->NLoaded()

protected:
  ETFQueue *fQueue; ///< queue for storing online events
  pthread_t fRecv, fSend; ///< the receive and send threads
  bool fIsRecvThAlive, fIsSendThAlive; ///< as the name indicates
  const DaqType fDaqType; ///< daq type: vme or pxi
  char fDaqC[16]; ///< daq character, "PXI" or "VME"

  struct sigaction fsa; ///< for taking care of SIGINT signal
};

#endif
