/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFServer
  \brief server for communication among different hosts on the Internet
  NOTE that this is an abstract class
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-21
  \date 2023-06-21 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-21   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef ETFServer_h
#define ETFServer_h

#include "ETFVServer.h"
#include <vector>

using std::vector;

class ETFReadShm;

class ETFServer : public ETFVServer{
public:
  ETFServer();
  /// \param shm: name of the input shm
  ETFServer(const string &ip, short port, const string &shm, DaqType daqType = kPXI,
    int nshm = 0);
  virtual ~ETFServer();

  virtual void send() override; ///< send to the other end of the Internet
  virtual void Close() override;
  void InitDaqShm(); ///< init the online_shm of the daq to the initial state

protected:
  int fNShm; ///< max nof shms for porting out
  vector<ETFReadShm *> fShms; ///< porting out to shms
};

#endif
