/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFYaml
  \brief a YAML user input reader
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2021-10-26
  \date 2021-12-04 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                   </tr>
  <tr>  <td>2021-10-26   <td>Asia Sun    <td>file created                  </tr>
  </table>

  \copyright Copyright (c) 2021-2023 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef ETFYaml_h
#define ETFYaml_h

#include <string>
#include <yaml-cpp/yaml.h>

using std::string;

class ETFYaml : public YAML::Node{
public:
  ETFYaml();
  ETFYaml(const string &file);
  ETFYaml(const YAML::Node &n);
  // ETFYaml(const ETFYaml &n); ///< copy ctor

  virtual ~ETFYaml();

  // ETFYaml &operator=(const ETFYaml &n);
};

#endif
