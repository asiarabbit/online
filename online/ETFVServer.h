/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFVServer
  \brief server for communication among different hosts on the Internet
  NOTE that this is an abstract class
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2023-06-10
  \date 2023-06-10 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2023-06-10   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef ETFVServer_h
#define ETFVServer_h

#include <netinet/in.h>
#include <string>
#include "ETFDaqData.h"

using std::string;

class ETFReadShm;

class ETFVServer : public ETFDaqData{
public:
  ETFVServer();
  /// create server with ip:port
  ETFVServer(const string &ip, short port = 5000, DaqType daqType = kPXI);
  virtual ~ETFVServer();

  virtual void Initialize(); ///< start the receiving thread

  /// estalbish the server and listen to linking requests
  virtual bool Online();
  virtual void Close(); ///< do some clean-ups
  int ServerFD() const{ return fds; }
  int ClientFD() const{ return fdc; }

  /// send to the other end of the Internet
  virtual void send() = 0;

  static const int kMAXCLI = 5; ///< max number of clients

protected:
  static void *avatar(void *arg); ///< thread func to read from LAN

  sockaddr_in fSock;
  string fIP;
  short fPort;
  int fds, fdc; ///< the file descriptor of the server sock and the client sock
  ETFReadShm *fShm0; ///< the shm feeding this server
};

#endif
