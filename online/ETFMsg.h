/**
  ETFAna project, Anyang Normal University && IMP-CAS
  \class ETFMsg
  \brief to print runtime message
  \author SUN Yazhou, asia.rabbit@163.com
  \since 2021-10-03
  \date 2023-06-28 last modified
  \attention
  changelog
  <table>
  <tr>  <th>Date         <th>Author      <th>Description                    </tr>
  <tr>  <td>2021-10-03   <td>Asia Sun    <td>file created                   </tr>
  </table>

  \copyright Copyright (c) 2021-2024 Anyang Normal U. && IMP-CAS with LGPLv3 LICENSE
*/

#ifndef ETFMsg_h
#define ETFMsg_h

#include <iostream>
#include <string>
#include "config.h"
#ifdef _ROOT_
#include <Rtypes.h>
#endif

using std::string;

class ETFMsg{
public:
	ETFMsg();
	virtual ~ETFMsg();
	/// \note the returned string will be changed at next call of this func
	/// so if you wanna use it in consecutive calls, please immediately use the
	/// result, or store it in a temporary memory before next call
	static const char *Form(const char *fmt, ...); ///< just like sprintf
	static void Info(const char *cname, const char *fmt, ...);
	static void Error(const char *cname, const char *fmt, ...);
	static void Warn(const char *cname, const char *fmt, ...);
	static void Debug(const char *cname, const char *fmt, ...);
	static void ConfigInfo(const char *cname, const char *fmt, ...);
	static void ShowPoint(const char *msg, const double *p, const int len = 3);
	static void Logo(std::ostream &os = std::cout); ///< ETFAna Logo
	static void LOGO(std::ostream &os = std::cout){ Logo(os); } ///< ETFAna Logo
	static void LogoPlain(string &s); ///< plain logo (without color)

	static void SetVerbose(bool opt = true){ fIsVerbose = opt; }
	static void SetSilent(bool opt = true){ fIsSilent = opt; }
	static void SetDebug(bool opt = true){ fIsDebug = opt; }
	static bool IsVerbose(){ return fIsVerbose; }
	static bool IsSilent(){ return fIsSilent; }
	static bool IsDebug(){ return fIsDebug; }

	/// \param isName (true): no comma in the returned string, suitable for path and file name
	static const char *time0(bool isName = false); ///< current time year-month-day_hour-Min
	static const char *sec0(bool isName = false); ///< current time year-month-day_hour-Min-Sec
	static time_t ms(); // in ms
	static time_t sec(){ return ms() / 1000; }
	static time_t ms1(); ///< time w.r.t the program execution time
	static bool Interrupted(){ return fIsInterrupted; }
	static bool irp(){ return fIsInterrupted; }
	static void SetInterrupted();
	static void SetIRP(){ return SetInterrupted(); }

#ifdef _ROOT_
	ClassDef(ETFMsg, 0);
#endif

protected:
	static char fMsg[512]; ///< a temporary string
	static bool fIsVerbose; ///< to switch On ConfigInfo() method
	static bool fIsSilent; ///< to switch off Info() method
	static bool fIsDebug; ///< to switch On Debug() method
	static bool fIsInterrupted; // SIGINT flag defined, triggered by ctrlc
};

#endif
