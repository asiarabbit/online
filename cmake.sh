#########################################################################
# File Name: cmake.sh
# Author: asia.rabbit
# mail: asia.rabbit@163.com
# Created Time: Tue 08 Aug 2023 01:21:27 PM CST
#########################################################################
#!/bin/bash
cmake ../online -DCMAKE_CXX_COMPILER=/home/asia/usr/bin/g++ \
        -DCMAKE_C_COMPILER=/home/asia/usr/bin/gcc

make -j10
